import paho.mqtt.client as mqtt
import time
from datetime import datetime
import os

broker_address = "localhost"
port = 3577

user = input("Input username: ")
client = mqtt.Client(user)
group = int(input("Make a group! How many person? (int) >>"))
pubtop = user

def on_message(client, userdata, message):
    msg = str(message.topic)
    time_now = datetime.now()
    time_string = time_now.strftime("%H:%M")
    if (msg != pubtop and msg != "photo"):
        print(f"{message.topic} > ", str(message.payload.decode("utf-8")), "(",time_string, ")")
    if (msg != pubtop and msg == "photo"):
        print("Messsage received")
        string = message.payload
        print("check")
        file = open("download.jpg", "wb")
        file.write(string)
        print(time_string)
        file.close()

client.on_message = on_message

def on_publish(client, userdata, result):
    pass
client.on_publish = on_publish

print("connecting to broker")
client.connect(broker_address, port)


client.loop_start()
for i in range(group -1):
    subtop = input("Member group: ")
    client.subscribe(subtop)
client.subscribe("photo")
print("subscribing")
os.system('clear')

print(f"--- {pubtop} --- ")
while True:
    time.sleep(1)
    chat = input()
    if (chat == "photo" or chat == "Photo" or chat == "PHOTO"):
        print("Photo name")
        choose = str(input("Input Photo name: "))
        print("uploading photo")
        file_upload = open(choose, "rb")
        content = file_upload.read()
        # pubfot = f"{pubtop} > Send Photo 1"
        
        client.publish("photo", content)
    client.publish(pubtop, chat.encode())

    time_now = datetime.now()
    time_string = time_now.strftime("%H:%M")
    pubchat = f"{pubtop} > " + chat
    if (chat == "photo" or chat == "Photo" or chat == "PHOTO"):
        pubchat = f"{pubtop} > Sent a photo"
    hist = open("history_chat.txt", "a")
    var = pubchat + " " + "(" + time_string + ")"
    hist.write(var + "\n")
    hist.close()

client.loop_stop()
