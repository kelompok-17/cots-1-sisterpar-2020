# Chatting

## Deskripsi Program
program ini dibuat untuk user agar dapat berkomunikasi dan mengirimkan photo 
secara group. Program ini dibuat menggunakan Indirect Communication, dengan 
menggunakan broker sebagai media penghubung antar port. Dalam program ini dapat
menyesuaikan berapa banyak orang yang dapat melakukan chat.

## Pembagian Tugas
Ni Luh Made Dita Anjani 
- Kerangka dasar group chat di dalam file pub-sub.py
- update final file pub-sub.py (menambah fitur write history chat ke txt dan 
tambah pilihan untuk upload foto)

Aldi Febiansyah
- Kerangka dasar upload dan download foto di dalam file pub-sub.py
- membuat readme

Ecky Prasetyo
- Membuat datetime pada group chat di dala file pub-sub.py

## Instalsi Program
1. Pastikan sudah menginstall mqtt
2. Jalankan broker pada port = 3577
3. Jalankan file pub-sub.py
4. Ulangi langkah 2 sampai jumlah member group chat yang diinginkan (disarankan
3 atau 4)
5. Masukkan username, enter lalu masukkan jumlah group chat (lakukan langkah ini
dulu pada semua terminal)
6. Masukkan username semua group member kecuali username pada terminal tersebut.
7. Setelah itu, bisa mulai chatting di dalam group.
8. Untuk upload foto, ketik "PHOTO" atau "Photo" atau "photo" di dalam inputan 
chat
9. Kemudian pilih foto (1/2/3)
10. Enter dan file photo masuk ke dalam "download.jpg"
11. Untuk history chat bisa langsung masuk ke dalam txt "history_chat.txt" di
dalam folder yang bersangkutan.
12. Jika ada error, pastikan semua username yang dimasukkan benar (tidak ada typo)

